<?php
require 'Entity.php';
require 'EntityManager.php';
require 'Human.php';
require 'Service.php';

echo "boostrapService\n";
Human::init();

echo "Request resourse\n";
$humanList = Human::find();

echo "Request individual resourse\n";
$russel = Human::findOne('FirstName eq \'Russell\'');

echo "Request by id\n";
try{
    $human = Human::findById('russellwhyte');
}catch(Exception $e){
    echo $e->getCode().' '.$e->getMessage();
}

echo "Query\n";
echo " - Query with OData filters\n";
$human = Human::find('Trips/any(d:d/Budget gt 3000)');

echo "New resourse\n";
$json = '{
    "@odata.type" : "Microsoft.OData.SampleService.Models.TripPin.Person",
    "UserName":"lewisblackkkk",
    "FirstName":"Lewis",
    "LastName":"Black",
    "Emails":[
        "lewisblack@example.com"
    ],
    "AddressInfo":[  
        {
            "Address":"187 Suffolk Ln.",
            "City":{
            "CountryRegion":"United States",
                "Name":"Boise",
                "Region":"ID"
            }
        }
    ],
    "Gender":"Male",
    "Concurrency":635519729375200400
}';
$andy = new Human(json_decode($json));
$andy->FirstName = 'Andy';
$andy->LastName = 'Anderson';
$responce = $andy->save();

echo "Get Metadata\n";
$metadata = Human::getMetadata();

echo "Update Resource\n";
$russel->FirstName = 'Nogathufugivero';
$russel->LastName = 'Tokirochikarimave';
$russel->save();

echo "Delete resource\n";
$json = '{
    "UserName":"lewisblack",
    "FirstName":"Lewis",
    "LastName":"Black",
    "Emails":[
        "lewisblack@example.com"
    ],
    "AddressInfo":[  
        {
            "Address":"187 Suffolk Ln.",
            "City":{
            "CountryRegion":"United States",
                "Name":"Boise",
                "Region":"ID"
            }
        }
    ],
    "Gender":"Male",
    "Concurrency":635519729375200400
}';
$johny = new Human(json_decode($json));
$johny->FirstName = 'Johny';
$johny->LastName = 'Demigavara';
$johny->save();
$johny->remove();

echo "Relating resourse\n";
//$andy->setRelated($russel);
//$andy->getRelated($russel);

echo "Removing resourse";
$andy->remove();