<?php

use \ODataMapper\EntityManager as EntityManager;
use \ODataMapper\Service as Service;

class Human extends \ODataMapper\Entity {

    public static $em;
    public static $key;
    public static $metadata;

    public static function init()
    {
        $em = static::initEm();
        static::$metadata = $doc = $em->getMetadata();
        $xpath = new \DOMXPath($doc);
        $xpath->registerNamespace('edmx', 'http://docs.oasis-open.org/odata/ns/edmx');
        $xpath->registerNamespace('edm', 'http://docs.oasis-open.org/odata/ns/edm');
        $keyNodes = $xpath->query('//edm:EntityType[@Name="Person"]/edm:Key/edm:PropertyRef/@Name');
        if(null != $keyNodes->item(0)) {
            $key = $keyNodes->item(0)->value;
            static::$key = $key;
        }
    }

    public static function initEm()
    {
        return static::$em = new EntityManager(
            new Service('services.odata.org/v4','(S(3q5m4dfnwfog3xmm4b0onadc))', 'TripPinServiceRW'),
            'People',
            'Human'
        );
    }
}