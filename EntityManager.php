<?php

namespace ODataMapper;

/**
 * Class EntityManager
 * @package ODataMapper
 * Built-in Filter Operations
    Operator Description Example
    Comparison Operators
        eq Equal Address/City eq 'Redmond'
        ne Not equal Address/City ne 'London'
        gt Greater than Price gt 20
        ge Greater than or equal Price ge 10
        lt Less than Price lt 20
        le Less than or equal Price le 100
        has Has flags Style has Sales.Color'Yellow'
    Logical Operators
        and Logical and Price le 200 and Price gt 3.5
        or Logical or Price le 3.5 or Price gt 200
        not Logical negation not endswith(Description,'milk')
    Arithmetic Operators
        add Addition Price add 5 gt 10
        sub Subtraction Price sub 5 gt 10
        mul Multiplication Price mul 2 gt 2000
        div Division Price div 2 gt 4
        mod Modulo Price mod 2 eq 0
    Grouping Operators
        ( ) Precedence grouping (Price sub 5) gt 10
    String Functions
        contains contains('Alfreds',CompanyName)
        endswith endswith(CompanyName,'Futterkiste')
        startswith startswith(CompanyName,'Alfr')
        length length(CompanyName) eq 19
        indexof indexof(CompanyName,'lfreds') eq 1
        substring substring(CompanyName,1) eq 'lfreds Futterkiste'
        tolower tolower(CompanyName) eq 'alfreds futterkiste'
        toupper toupper(CompanyName) eq 'ALFREDS FUTTERKISTE'
        trim trim(CompanyName) eq 'Alfreds Futterkiste'
        concat concat(concat(City,', '), Country) eq 'Berlin, Germany'
    Date Functions
        year year(BirthDate) eq 0
        month month(BirthDate) eq 12
        day day(StartTime) eq 8
        hour hour(StartTime) eq 1
        minute minute(StartTime) eq 0
        second second(StartTime) eq 0
        fractionalseconds second(StartTime) eq 0
        date date(StartTime) ne date(EndTime)
        time time(StartTime) le StartOfDay
        totaloffsetminutes totaloffsetminutes(StartTime) eq 60
        now StartTime ge now()
        mindatetime StartTime eq mindatetime()
        maxdatetime EndTime eq maxdatetime()
    Math Functions
        round round(Freight) eq 32
        floor floor(Freight) eq 32
        ceiling ceiling(Freight) eq 33
        Type Functions
        cast cast(ShipCountry,Edm.String)
        isof isof(NorthwindModel.Order)
        isof isof(ShipCountry,Edm.String)
    Geo Functions
        geo.distance geo.distance(CurrentPosition,TargetPosition)
        geo.length geo.length(DirectRoute)
        geo.intersects geo.intersects(Position,TargetArea)
 */

class EntityManager {

    /**
     * @var Service
     */
    private $service;
    private $resourceName;
    private $entityClassName;

    public function __construct(Service $service, $resourceName, $entityClassName)
    {
        $this->service = $service;
        $this->resourceName = $resourceName;
        $this->entityClassName = $entityClassName;
    }

    public function find($filter = null)
    {
        $query = $this->resourceName;
        if($filter) {
            $encodedFilter = rawurlencode($filter);
            $query.='?$filter='.$encodedFilter;
        }
        $response = $this->service->request($query);
        if(strlen($response[0]) > 0){
            $collection = [];
            $responseObject = json_decode($response[0]);
            foreach ($responseObject->value as $data){
                $collection[] = new $this->entityClassName($data, true);
            }
            return $collection;
        } else {
            throw new \Exception('No entity found for query');
        }
    }

    public function findOne($filter = null)
    {
        $encodedFilter = rawurlencode($filter);
        $query = $this->resourceName.'?$filter='.$encodedFilter.'&$top=1';
        $response = $this->service->request($query);
        if(strlen($response[0]) > 0){
            $responseObject = json_decode($response[0]);
            if(null === $responseObject->value[0]) throw new \Exception('No entity found for query');
            $data = $responseObject->value[0];
            return new $this->entityClassName($data, true);
        } else {
            throw new \Exception('No entity found for query');
        }
    }

    public function findById($id)
    {
        $query = $this->resourceName.'(\''.$id.'\')';
        $response = $this->service->request($query);
        if(strlen($response[0]) > 0){
            $responseObject = json_decode($response[0]);
            $data = $responseObject;
            return new $this->entityClassName($data, true);
        }  else {
            throw new \Exception('No entity found for query');
        }
    }

    public function save(Entity $entity)
    {
        $resourceName = $this->resourceName . '(\'' . $entity->{$entity::$key} . '\')';
        $request = $entity->persist ?
            $this->service->request($resourceName, (string) $entity, false, 'UPDATE') :
            $this->service->request($this->resourceName, (string) $entity, true);
        return $request;
    }

    public function getRelated($id){}

    public function setRelated(){}

    public function remove(Entity $entity)
    {
        $resourceName = $this->resourceName . '(' . $entity->{$entity::$key} . ')';
        $response = $this->service->request($resourceName, null, false, 'DELETE');
        $entity->persist = false;
        return $response;
    }

    /**
     * @return \DOMDocument
     * @throws \Exception
     */
    public function getMetadata()
    {
        $response = $this->service->request('$metadata');
        $xml = $response[0];
        $doc = new \DOMDocument($xml);
        $doc->loadXml($xml);
        return $doc;
    }
}