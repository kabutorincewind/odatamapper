<?php

namespace ODataMapper;


abstract class Entity
{
    public static $em;
    public static $key;
    public static $metadata;

    public $id;
    public $data;
    public $persist = false;

    public function __construct(\stdClass $data = null, $persist = false)
    {
        $data != null ? $this->data = $data : $this->data = new \stdClass();
        $this->persist = $persist;
    }

    /**
     * Need to be implemented to set em, key and metadata variables 
     */
    public static function init(){
        throw new \Exception('Method init must be implemented');
    }

    /**
     * @return \ODataMapper\EntityManager
     */
    public static function getEm(){
        if (!isset(self::$em)) static::initEm();
        return static::$em;
    }

    /**
     * @return \DOMDocument
     */
    public static function getMetadata()
    {
        $metadata = static::getEm()->getMetadata();
        return $metadata;
    }
    
    function __get($name)
    {
        if(isset($this->data->$name)) return $this->data->$name;
    }

    function __set($name, $value)
    {
        return $this->data->$name = $value;
    }

    function __toString()
    {
        return json_encode($this->data);
    }

    public function save()
    {
        return static::getEm()->save($this);
    }

    public function remove()
    {
        return static::getEm()->remove($this);
    }

    public static function find($filter = null)
    {
        return static::getEm()->find($filter);
    }

    public static function findOne($filter = null)
    {
        return static::getEm()->findOne($filter);
    }

    public static function findById($id)
    {
        return static::getEm()->findById($id);
    }

    public function getRelated()
    {
        //return static::getEm()->getRelated();
    }

    public function setRelated()
    {
        //return static::getEm()->setRelated();
    }
}

