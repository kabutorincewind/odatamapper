<?php

namespace ODataMapper;


class Service {

    protected $host;
    protected $token;
    protected $serviceName;
    protected $metadata;

    public function __construct($host, $token, $serviceName)
    {
        $this->host = $host;
        $this->token = $token;
        $this->serviceName = $serviceName;
    }

    public function getUrl($token = false){
        if(!$token) return 'http://'.$this->host.'/'.$this->serviceName;
        return 'http://'.$this->host.'/'.$this->token.'/'.$this->serviceName;
    }

    public function request($query = null, $data = null, $token = false, $customRequest = false)
    {
        try {       
            if(!function_exists('curl_init')) throw new \Exception('No curl extension installed');
            $ch = curl_init();
            $opts = [
                CURLOPT_URL => $this->getUrl($token) . '/' . $query,
                CURLOPT_HTTP_VERSION => '1.1',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HTTPHEADER => [
                    'OData-Version: 4.0',
                    'OData-MaxVersion: 4.0'
                ],
                CURLOPT_FOLLOWLOCATION => true
            ];
            if ($data !== null) {
                $opts += [
                    CURLOPT_POST => true,
                    CURLOPT_POSTFIELDS => $data
                ];
                $opts[CURLOPT_HTTPHEADER][] = 'Content-Length:' . strlen($data);
                $opts[CURLOPT_HTTPHEADER][] = 'Content-Type:application/json;odata.metadata=minimal;odata.streaming=true;IEEE754Compatible=false';
            }
            if ($customRequest !== false) {
                $opts[CURLOPT_CUSTOMREQUEST] = $customRequest;
            }
            curl_setopt_array($ch, $opts);
            $response = curl_exec($ch);
            if ($response == false && "0" != curl_errno($ch)) throw new \Exception(curl_errno($ch));
            $info = curl_getinfo($ch);
            return [$response, $info];
        } catch (\Exception $e) {var_dump($e->getMessage()); exit;}
    }
}